# Prueba de manejo de proyectos en Elixir y Phoenix

## Contenidos
1. [Sobre el proyecto](#sobre-el-proyecto)
2. [Instalacion](#instalacion)
2. [Objetivos](#objetivos)

## Sobre el proyecto
Este proyecto es un fork del proyecto [Remote Retro](https://github.com/stride-nyc/remote_retro),
una herramienta Open Source para manejar las retrospectivas de un equipo Ágil.

## Instalación

### Dependencias
El proyecto tiene las siguientes dependencias para poder correr:
- Elixir 1.6.5
- NodeJS >= 8.12.0
- PostgreSQL >= 11.2

Para correr el proyecto se debe hacer:
```bash
# Instalar dependencias de Elixir
mix deps.get

# Instalar dependencias de NodeJS
npm install # o yarn

# Crear la base de datos y hacer las migraciones
mix ecto.create && mix ecto.migrate
MIX_ENV=test mix ecto.create && mix ecto.migrate
```

Luego se puede correr los tests con:
```bash
# Elixir Unit Tests
mix test

# End-to-end tests
mix e2e

# JS tests
yarn test
```

Finalmente, para correr el servidor:
```bash
MIX_ENV=dev mix phx.server
```

Para poder hacer login con usuarios es necesario generar una llave de Google Auth,
para mas información consulte el README del proyecto original en `README.orig.md`.

## Objetivos
El objetivo de esta tarea es implementar características nuevas encima de este
código. Lo que se espera que el candidato pueda hacer es:
- Entender una base de código ajena escrita en Elixir y el framework web Phoenix.
- Capacidad de implementar funcionalidades de manera ordenada usando testing y buenas prácticas.

Las funcionalidades a implementar son las siguientes:

1. Crear un endpoint REST que entregue el listado completo de usuarios registrados
como un JSON cuando se le hace GET.
